%% Sampling time [s]
Ts_ = 1e-4;

%% IcePAP Steps and position reset values [nm]
pos_reset = 0;

%% Trigger Input - 0 for input 1, 1 for input 2
trig_input = 0;

%% Counters
counters_ = [
    struct('name', 'exc_fjp', 'unit', 'V',   'description', 'Test signal for identification purposes (piezo)'), ...
    struct('name', 'exc_pos', 'unit', 'nm',  'description', 'Test signal for identification purposes (closed-loop)'), ...
    struct('name', 'u_lac',   'unit', 'V',   'description', 'Output of the LAC controller'), ...
    struct('name', 'u_hac',   'unit', 'V',   'description', 'Output of the HAC controller'), ...
    struct('name', 'u_tot',   'unit', 'V',   'description', 'Total command signal'), ...
    struct('name', 'interf',  'unit', 'nm',  'description', 'FastJack position measured by interferometer'), ...
    struct('name', 'steps',   'unit', 'step','description', 'Generated steps by IcePAP'), ...
    struct('name', 'force',   'unit', 'V',   'description', 'Measured voltage on Force Sensor'), ...
    struct('name', 'error_interf', 'unit', 'nm', 'description', 'Measured position Error (interf)'), ...
];

%% Add Filtered Counters
filtered_counters_ = {'interf', 'error_interf'};
for counter_name_ = filtered_counters_
    filtered_counter_ = counters_(ismember({counters_.name}, counter_name_));
    filtered_counter_.name = strcat(filtered_counter_.name, '_filtered');
    filtered_counter_.description = strcat(filtered_counter_.description, ' - Filtered');
    counters_ = [counters_, filtered_counter_];
end